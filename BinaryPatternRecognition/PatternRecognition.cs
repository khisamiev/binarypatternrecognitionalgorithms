﻿using CadreManager;
using FastAlgorithm;
using StandardAlgorithm;
using SymbolDateBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryPatternRecognition
{
    class PatternRecognition
    {
        static void Main(string[] args)
        {
            new PatternRecognition();
        }

        public const string CADR_NUMS = @"../../../img/cadr_standard.txt";
        public const string CADR_NUMS_FAST = @"../../../img/cadr_fast.txt";
        public PatternRecognition()
        {
            bool a = CompareTwoFiles(CADR_NUMS_FAST, CADR_NUMS);
           // DoFast();
           /* SymbolUnit s = new SymbolUnit() 
            { 
                Array = new int[,] 
                { 
                    //{1,1,1},{0,0,1},{1,1,1},{1,0,0},{1,1,1}
                    {1,1,1},{0,0,1},{1,1,1},{0,0,1},{1,1,1}
                }
            };

            Fast f = new Fast();
            int k = f.RecognizeSymbol(s);
            */
            Console.WriteLine("");
        }
        public void DoFast()
        {
            Fast f = new Fast();
            StringBuilder sB = new StringBuilder("");
            int lineCount = Generator.BITMAP_HEIGHT / Symbol.SYMBOL_H;

            for (int i = 0; i < lineCount; i++)
            {
                List<SymbolUnit> list = CadreManipulation.GetLineSymbols(i);

                for (int j = 0; j < list.Count; j++)
                {
                    int k = f.RecognizeSymbol(list[j]);
                    sB.Append(k.ToString());
                }
            }
            Generator.SaveTextToFile(sB.ToString(), CADR_NUMS_FAST);
        }

        public bool CompareTwoFiles(string file1, string file2)
        {
            String[] linesA = File.ReadAllLines(file1);
            String[] linesB = File.ReadAllLines(file2);

            if (linesA[0].Equals(linesB[0])) return true;
            return false;
        }
        public void GenerateCadre()
        {
            CadreManager.Generator cadrGen = new CadreManager.Generator();
            cadrGen.GenerateCadr();
        }
        public void DoStandard()
        {
            Standard s = new Standard();
            StringBuilder sB = new StringBuilder("");
            int lineCount = Generator.BITMAP_HEIGHT / Symbol.SYMBOL_H;

            for (int i = 0; i < lineCount; i++)
            {
                List<SymbolUnit> list = CadreManipulation.GetLineSymbols(i);

                for (int j = 0; j < list.Count; j++)
                {
                    int k = s.RecognizeSymbol(list[j]);
                    sB.Append(k.ToString());
                }
            }
            Generator.SaveTextToFile(sB.ToString(), CADR_NUMS);
        }
    }
}

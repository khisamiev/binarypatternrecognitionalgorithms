﻿using Helpers;
using SymbolDateBase;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadreManager
{
    public class CadreManipulation
    {
        public static string PATH_TO_BINARY_CADR = Helper.GetRootPath() + @"img/" + Generator.BIN_IMG_NAME;
        public CadreManipulation()
        {
        }

        public List<SymbolUnit> GetLineSymbols(int lineIndex)
        {
            int w = Generator.SYMBOL_WIDTH;
            int h = Generator.SYMBOL_HEIGHT;

            if (!File.Exists(PATH_TO_BINARY_CADR))
            {
                return null;
            }

            using (Bitmap cadr = new Bitmap(PATH_TO_BINARY_CADR))
            {
                Rectangle symbolArea = new Rectangle(0, 0, w, h);
                PixelFormat format = cadr.PixelFormat;

                List<SymbolUnit> units = new List<SymbolUnit>() { };
                Bitmap symbolBitmap;
                int i = lineIndex * h;

                for (int j = 0; j < cadr.Width; j += w)
                {
                    symbolArea = new Rectangle(j, i, w, h);

                    symbolArea.X = j;
                    symbolArea.Y = i;

                    symbolBitmap = cadr.Clone(symbolArea, format);

                    SymbolUnit symbol = Symbol.BitmapToSymbol(symbolBitmap);

                    units.Add(symbol);
                }
                return units;
            }
        }

    }
}

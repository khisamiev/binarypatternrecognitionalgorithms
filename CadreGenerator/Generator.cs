﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadreManager
{
    public class Generator
    {
        
        public const int SYMBOL_HEIGHT = 5;
        public const int SYMBOL_WIDTH = 3;

        public static int OBJECT_COUNT_X = 100;
        public static int OBJECT_COUNT_Y = 100;

        public static int OBJECT_COUNT = OBJECT_COUNT_X * OBJECT_COUNT_Y;

        public static int BITMAP_WIDTH = OBJECT_COUNT_X * SYMBOL_WIDTH;
        public static int BITMAP_HEIGHT = OBJECT_COUNT_Y * SYMBOL_HEIGHT;

        public const string IMG_NAME = "cadr.jpg";
        public const string FILE_NAME = "cadr.txt";
        public const string BIN_IMG_NAME = "cadr_binary.jpg";

        public static string CADR_PATH = Helper.GetRootPath() + @"img/" + IMG_NAME;
        public static string CADR_NUMS = Helper.GetRootPath() + @"img/" + FILE_NAME;
        public static string CADR_BINARY_PATH = Helper.GetRootPath() + @"img/" + BIN_IMG_NAME;


        public Generator(Point p) 
        {
            OBJECT_COUNT_X = p.X;
            OBJECT_COUNT_Y = p.Y;

            BITMAP_WIDTH = OBJECT_COUNT_X * SYMBOL_WIDTH;
            BITMAP_HEIGHT = OBJECT_COUNT_Y * SYMBOL_HEIGHT;
        }
        public void GenerateCadr()
        {
            using (Bitmap b = new Bitmap(BITMAP_WIDTH, BITMAP_HEIGHT))
            using (Graphics g = Graphics.FromImage(b))
            {
                Random r = new Random();
                int symbolCount = SymbolDateBase.Symbol.SYMBOL_COUNT;
                int max = 100 * symbolCount;
                StringBuilder sB = new StringBuilder("");

                for (int i = 0; i < b.Height; i += SYMBOL_HEIGHT)
                {
                    for (int j = 0; j < b.Width; j += SYMBOL_WIDTH)
                    {
                        int index = r.Next(symbolCount);
                        sB.Append(index.ToString());

                        Image img = SymbolDateBase.Symbol.GetImageSymbol(index, false);
                        g.DrawImageUnscaled(img, j, i, SymbolDateBase.Symbol.SYMBOL_W, SymbolDateBase.Symbol.SYMBOL_H);
                    }
                }
                Helper.SaveTextToFile(sB.ToString(), CADR_NUMS);
                b.Save(CADR_PATH);
                CreateBinaryCadr();
            }
        }

        public void CreateBinaryCadr()
        {
            using (Bitmap b = new Bitmap(CADR_PATH))
            using (Graphics g = Graphics.FromImage(b))
            {

                for (int i = 0; i < b.Height; i ++)
                {
                    for (int j = 0; j < b.Width; j ++)
                    {
                        if (b.GetPixel(j, i) != Color.FromArgb(255,255,255,255))
                        {
                            b.SetPixel(j, i, Color.Black);
                        }
                    }
                }
                b.Save(CADR_BINARY_PATH);
            }
        }
    }
}

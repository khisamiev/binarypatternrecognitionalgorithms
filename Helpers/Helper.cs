﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public class Helper
    {
        public static bool SaveTextToFile(string strData, string FullPath)
        {
            bool bAns = false;
            try
            {
                if (File.Exists(FullPath))
                {
                    System.IO.File.WriteAllText(FullPath, strData);
                    bAns = true;
                }
                else
                {
                    System.IO.File.Create(FullPath).Close();
                    System.IO.File.AppendAllText(FullPath, strData);
                    bAns = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bAns;
        }

        public static bool CompareTwoFiles(string file1, string file2)
        {
            String[] linesA = File.ReadAllLines(file1);
            String[] linesB = File.ReadAllLines(file2);

            if (linesA[0].Equals(linesB[0]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GetRootPath()
        {
            string path = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));

            int index = path.LastIndexOf(@"\");

            return  path.Remove(index + 1);
        }
    }
}

﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SymbolDateBase
{
    public static class Symbol
    {
        public static int SYMBOL_H = 5;
        public static int SYMBOL_W = 3;

        public static int SYMBOL_COUNT = 10;

        public static string PATH_TO_SYMBOLS = Helper.GetRootPath() + @"SymbolDateBase\Database\";

        public static Bitmap GetImageSymbol(int index, bool isBlackWhiteMode)
        {
            Bitmap symbol = new Bitmap(SYMBOL_W, SYMBOL_H);
            int[,] arr = GetSymbolArray(index);

            Color color = GetColor(index, isBlackWhiteMode);

            for (int i = 0; i < symbol.Height; i++)
            {
                for (int j = 0; j < symbol.Width; j++)
                {
                    if (arr[i, j] == 1)
                    {
                        symbol.SetPixel(j, i, color);
                    }
                    else
                    {
                        symbol.SetPixel(j, i, Color.White);
                    }
                }
            }
            return symbol;
        }

        public static SymbolUnit BitmapToSymbol(Bitmap symbolBitmap)
        {
            SymbolUnit unit = new SymbolUnit();
            Color c;
            for (int i = 0; i < symbolBitmap.Height; i++)
            {
                for (int j = 0; j < symbolBitmap.Width; j++)
                {
                    c = symbolBitmap.GetPixel(j, i);
                    if (c != Color.FromArgb(255, 255, 255, 255))
                    {
                        unit.Array[i, j] = 1;
                    }
                    else
                    {
                        unit.Array[i, j] = 0;
                    }
                }
            }
            return unit;
        }

        public static int[,] GetSymbolArray(int i)
        {
            string fileDir = System.IO.Path.GetFullPath(PATH_TO_SYMBOLS);
            string fileName = "Symbol_" + i + ".txt";

            string filePath = fileDir + fileName;

            int[,] symbolArr = new int[SYMBOL_H, SYMBOL_W];

            StreamReader file = new StreamReader(filePath);

            string line = "";
            int counter = 0;

            while ((line = file.ReadLine()) != null)
            {
                char[] charArray = line.ToCharArray();
                int[] arr = Array.ConvertAll(charArray, c => (int)Char.GetNumericValue(c));

                for (int k = 0; k < arr.Length; k++)
                {
                    symbolArr[counter, k] = arr[k];
                }
                counter++;
            }
            return symbolArr;
        }

        private static Color GetColor(int i, bool isBlackWhiteMode)
        {
            Color color = Color.Black;

            if (!isBlackWhiteMode)
            {
                switch (i)
                {
                    case 0: { color = Color.Red; break; }
                    case 1: { color = Color.Green; break; }
                    case 2: { color = Color.Blue; break; }
                    case 3: { color = Color.Brown; break; }
                    case 4: { color = Color.DarkGreen; break; }
                    case 5: { color = Color.DarkViolet; break; }
                    case 6: { color = Color.GreenYellow; break; }
                    case 7: { color = Color.Aqua; break; }
                    case 8: { color = Color.Cyan; break; }
                    case 9: { color = Color.Pink; break; }
                }
            }
            return color;
        }

        

    }
}

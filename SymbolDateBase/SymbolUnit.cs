﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SymbolDateBase
{
    public class SymbolUnit
    {
        public int[,] Array { get; set; }

        public SymbolUnit()
        {
            Array = new int[Symbol.SYMBOL_H, Symbol.SYMBOL_W];
        }
    }
}

﻿using Helpers;
using SymbolDateBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StandardAlgorithm
{
    public class Standard
    {
        List<int[,]> maskedEtalonList;
        public static string PATH_TO_INCIDENCE_MASKS = Helper.GetRootPath() + @"StandardAlgorithm\Masks\";
        public Standard()
        {
            int[,] mask_0 = GetMaskSymbolArray(0);
            int[,] mask_1 = GetMaskSymbolArray(1);
            int[,] mask_2 = GetMaskSymbolArray(2);
            int[,] mask_3 = GetMaskSymbolArray(3);
            int[,] mask_4 = GetMaskSymbolArray(4);
            int[,] mask_5 = GetMaskSymbolArray(5);
            int[,] mask_6 = GetMaskSymbolArray(6);
            int[,] mask_7 = GetMaskSymbolArray(7);
            int[,] mask_8 = GetMaskSymbolArray(8);
            int[,] mask_9 = GetMaskSymbolArray(9);

            int[,] etalon_0 = Symbol.GetSymbolArray(0);
            int[,] etalon_1 = Symbol.GetSymbolArray(1);
            int[,] etalon_2 = Symbol.GetSymbolArray(2);
            int[,] etalon_3 = Symbol.GetSymbolArray(3);
            int[,] etalon_4 = Symbol.GetSymbolArray(4);
            int[,] etalon_5 = Symbol.GetSymbolArray(5);
            int[,] etalon_6 = Symbol.GetSymbolArray(6);
            int[,] etalon_7 = Symbol.GetSymbolArray(7);
            int[,] etalon_8 = Symbol.GetSymbolArray(8);
            int[,] etalon_9 = Symbol.GetSymbolArray(9);

            maskedEtalonList = new List<int[,]>() { };

            maskedEtalonList.Add(Masking(etalon_0, mask_0));
            maskedEtalonList.Add(Masking(etalon_1, mask_1));
            maskedEtalonList.Add(Masking(etalon_2, mask_2));
            maskedEtalonList.Add(Masking(etalon_3, mask_3));
            maskedEtalonList.Add(Masking(etalon_4, mask_4));
            maskedEtalonList.Add(Masking(etalon_5, mask_5));
            maskedEtalonList.Add(Masking(etalon_6, mask_6));
            maskedEtalonList.Add(Masking(etalon_7, mask_7));
            maskedEtalonList.Add(Masking(etalon_8, mask_8));
            maskedEtalonList.Add(Masking(etalon_9, mask_9));

        }

        public int RecognizeSymbol(SymbolUnit symbol)
        {
            for (int i = 0; i < Symbol.SYMBOL_COUNT; i++)
            {
                if (IsRecognized(symbol.Array, maskedEtalonList[i]))
                {
                    return i;
                }
            }

            return -1;
        }
        private bool IsRecognized(int[,] symbol, int[,] masked_etalon)
        {
            bool isRecoznize = true;

            for (int i = 0; i < Symbol.SYMBOL_H; i++)
            {
                for (int j = 0; j < Symbol.SYMBOL_W; j++)
                {
                    if (masked_etalon[i, j] != 2)
                    {
                        if (symbol[i, j] != masked_etalon[i, j])
                        {
                            isRecoznize = false;
                        }
                    }
                }
            }

            return isRecoznize;
        }

        private int[,] Masking(int[,] symbol, int[,] mask)
        {
            for (int i = 0; i < Symbol.SYMBOL_H; i++)
            {
                for (int j = 0; j < Symbol.SYMBOL_W; j++)
                {
                    symbol[i, j] = mask[i, j] == 0 ? symbol[i, j] : 2;
                }
            }
            return symbol;
        }

        private int[,] GetMaskSymbolArray(int i)
        {
            string fileDir = PATH_TO_INCIDENCE_MASKS;
            string fileName = "Symbol_" + i + ".txt";

            string filePath = fileDir + fileName;

            int[,] symbolArr = new int[Symbol.SYMBOL_H, Symbol.SYMBOL_W];

            StreamReader file = new StreamReader(filePath);

            string line = "";
            int counter = 0;

            while ((line = file.ReadLine()) != null)
            {
                char[] charArray = line.ToCharArray();
                int[] arr = Array.ConvertAll(charArray, c => (int)Char.GetNumericValue(c));

                for (int k = 0; k < arr.Length; k++)
                {
                    symbolArr[counter, k] = arr[k];
                }
                counter++;
            }
            return symbolArr;
        }
    }
}

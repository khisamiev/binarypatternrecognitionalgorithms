﻿using CadreManager;
using FastAlgorithm;
using SymbolDateBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StandardAlgorithm;
using Helpers;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Main
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const string CADR_NUMS = @"../../../img/cadr_standard.txt";
        public const string CADR_NUMS_FAST = @"../../../img/cadr_fast.txt";

        public static string ROOT_PATH = Helper.GetRootPath();

        public Point CadrObjectCount = new Point(100,100); 

        public MainWindow()
        {
            InitializeComponent();

            Init();

            Start();
        }
        public void Init()
        {
        }

        public void Start()
        {
            //GenerateCadre();
            DoFast();
            DoStandard();
            Console.ReadLine();
            //System.Diagnostics.Process.Start(@"C:\Users\lenar_000\Documents\Visual Studio 2013\Projects\Laba_Recognition\binarypatternrecognitionalgorithms\img\cadr.jpg");
        }
        public void DoFast()
        {
            Fast f = new Fast();
            StringBuilder sB = new StringBuilder("");
            int lineCount = Generator.BITMAP_HEIGHT / Symbol.SYMBOL_H;

            CadreManipulation cadrMan = new CadreManipulation();

            Stopwatch stopwatch = new Stopwatch();
            long total_time_MS = 0;
            long time_MS = 0;
            for (int i = 0; i < lineCount; i++)
            {
                List<SymbolUnit> list = cadrMan.GetLineSymbols(i);

                for (int j = 0; j < list.Count; j++)
                {
                    int k = f.RecognizeSymbol(list[j], stopwatch, out time_MS);

                    stopwatch.Stop();
                    total_time_MS += time_MS;

                    sB.Append(k.ToString());
                }
            }
            Console.WriteLine("Fast : {0}", total_time_MS);
            Helper.SaveTextToFile(sB.ToString(), CADR_NUMS_FAST);
        }


        public void GenerateCadre()
        {
            CadreManager.Generator cadrGen = new CadreManager.Generator(new System.Drawing.Point(100,100));
            cadrGen.GenerateCadr();
        }
        public void DoStandard()
        {
            StandardAlgorithm.Standard s = new StandardAlgorithm.Standard();
            StringBuilder sB = new StringBuilder("");
            int lineCount = Generator.BITMAP_HEIGHT / Symbol.SYMBOL_H;
            CadreManipulation cadrMan = new CadreManipulation();

            Stopwatch stopwatch = new Stopwatch();
            long time_MS = 0;
            for (int i = 0; i < lineCount; i++)
            {
                List<SymbolUnit> list = cadrMan.GetLineSymbols(i);
               
                for (int j = 0; j < list.Count; j++)
                {
                    stopwatch.Reset();
                    stopwatch.Start();

                    int k = s.RecognizeSymbol(list[j]);

                    stopwatch.Stop();
                    time_MS += stopwatch.ElapsedTicks;

                    sB.Append(k.ToString());
                }
            }

            Console.WriteLine("Standard : {0}" , time_MS);
            Helper.SaveTextToFile(sB.ToString(), CADR_NUMS);
        }

        private void Btn_x1_Click(object sender, RoutedEventArgs e)
        {
            CadrObjectCount = new Point(100,100);
            this.Tb_total.Text = "= " + 10000 +" объ.";
        }

        private void Btn_x2_Click(object sender, RoutedEventArgs e)
        {
            CadrObjectCount = new Point(100, 200);
            this.Tb_total.Text = "= " + 20000 + " объ.";
        }

        private void Btn_x4_Click(object sender, RoutedEventArgs e)
        {
            CadrObjectCount = new Point(200, 200);
            this.Tb_total.Text = "= " + 40000 + " объ.";
        }

        private void Btn_x5_Click(object sender, RoutedEventArgs e)
        {
            CadrObjectCount = new Point(100, 500);
            this.Tb_total.Text = "= " + 50000 + " объ.";
        }

        private void Btn_x10_Click(object sender, RoutedEventArgs e)
        {
            CadrObjectCount = new Point(500, 500);
            this.Tb_total.Text = "=10^5 объ.";
        }
    }
}

﻿using Helpers;
using SymbolDateBase;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastAlgorithm
{
    public class Fast
    {
        public const int INCEDENCE_MATRIX_H = 10;
        public const int INCEDENCE_MATRIX_W = 8;

        public string PATH_TO_INCIDENCE_MATRIXS = Helper.GetRootPath() + @"FastAlgorithm\IncidenceMatrixs\";

        public static int[] kk = new int[10] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        public static int[,] tmp;

        List<int[,]> incidence_List;
        public Fast()
        {
            incidence_List = new List<int[,]>();

            incidence_List.Add(GetIncedenceArray(0));
            incidence_List.Add(GetIncedenceArray(1));
            incidence_List.Add(GetIncedenceArray(2));
            incidence_List.Add(GetIncedenceArray(3));
            incidence_List.Add(GetIncedenceArray(4));

        }

        private int[,] GetIncedenceArray(int i)
        {
            string fileDir = PATH_TO_INCIDENCE_MATRIXS;
            string fileName = "Incidence_" + i + ".txt";

            string filePath = fileDir + fileName;

            int[,] symbolArr = new int[INCEDENCE_MATRIX_H, INCEDENCE_MATRIX_W];

            StreamReader file = new StreamReader(filePath);

            string line = "";
            int counter = 0;

            while ((line = file.ReadLine()) != null)
            {
                char[] charArray = line.ToCharArray();
                int[] arr = Array.ConvertAll(charArray, c => (int)Char.GetNumericValue(c));

                for (int k = 0; k < arr.Length; k++)
                {
                    symbolArr[counter, k] = arr[k];
                }
                counter++;
            }
            return symbolArr;
        }

        public int RecognizeSymbol(SymbolUnit symbol, Stopwatch stopwatch, out long timeSpan)
        {        
            kk = new int[10] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
            int z = 0;
            stopwatch.Reset();
            stopwatch.Start();

            for (int i = 0; i < Symbol.SYMBOL_H; i++)
            {
                z = 4 * symbol.Array[i, 0] + 2 * symbol.Array[i, 1] + symbol.Array[i, 2];

                tmp = incidence_List[i];

                for (int j = 0; j < INCEDENCE_MATRIX_H; j++)
                {
                    kk[j] *= tmp[j,z];
                }
            }

            stopwatch.Stop();
            timeSpan=stopwatch.ElapsedTicks;

            for (int i = 0; i < INCEDENCE_MATRIX_H; i++)
            {
                if (kk[i] == 1) return i;
            }

            return -1;
        }
    }
}
